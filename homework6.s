	.global _start
@ Display your first and last name and the last four digits of your student ID.
@   Example: Tim McGowen 1234
_start:
	MOV R7, #4
	MOV R0, #1
	MOV R2, #15
	LDR R1, =name
	SWI 0


@ Prompt the user for the last four digits of their student ID.
@   Example: Please enter the last four digits of your student ID:
_prompt:
	MOV R7, #4
        MOV R0, #1
        MOV R2, #41
        LDR R1, =prompt
        SWI 0
 @ Get input from the keyboard.
_input:
	MOV R7, #3
	MOV R0, #0
	MOV R2, #5
	LDR R1, =id
	SWI 0
@ Validate that four characters were entered
_valid:
	LDR R1, =id
	LDRB R2, [R1, #4]
	CMP R2, #10
	BNE _error
	BEQ _sumNchk
 @ If four characters were not entered then display an error message.
    @  The error message should display "You did not enter 4 digits."
_error:
	MOV R7, #4
	MOV R0, #1
	MOV R2, #28
	LDR R1, =error
	SWI 0
	B _exit
    @ Else you have four characters then
_sumNchk:    @ Take each of the ASCII digits, convert to decimal and add together. 
	MOV R6, #48
	LDR R1, =id
	LDRB R2, [R1]
	RSB R2, R6, R2
	LDRB R3, [R1, #1]
	RSB R3, R6, R3
	LDRB R4, [R1, #2]
	RSB R4, R6, R4
	LDRB R5, [R1, #3]
	RSB R5, R6, R5
	ADD R1, R2, R3
	ADD R1, R1, R4
	ADD R1, R1, R5
	CMP R1, #18
	BLT _less        @ If the value is less than 18 display the letter L.
        BEQ _equal        @ If the value equals 18 display the letter E.
        BGT _great        @ If the value is greater than 18 display the letter G.
_less:
	LDR R0, =result
        MOV R1, #'L'
	MOV R3, #10
        STR R1, [R0]
	STR R3, [R0, #1]
        B _result
_equal:
	LDR R0, =result
        MOV R1, #'E'
	MOV R3, #10
        STR R1, [R0]
	STR R3, [R0, #1]
        B _result
_great:
	LDR R0, =result
	MOV R1, #'G'
	MOV R3, #10
	STR R1, [R0]
	STR R3, [R0, #1]
	B _result
_result:
	MOV R7, #4
        MOV R0, #1
        MOV R2, #2
        LDR R1, =result
        SWI 0
_exit:
	MOV R7, #1
	SWI 0

.data
name:
.ascii "Mark Dyer 7591\n"
prompt:
.ascii "Please Enter the last 4 digits of you ID\n"
error:
.ascii "You did not enter 4 digits.\n"
id:
.ascii "     "
result:
.ascii "  "

